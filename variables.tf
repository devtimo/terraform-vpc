variable "name" {
  description = "Nome da VPC"
}

variable "cidr" {
  description = "CIDR da VPC"
  default = "10.0.0.0/16"
}

variable "azs" {
  description = "Zonas de disponibilidades"
  type = list(string)
  default = ["sa-east-1a", "sa-east-1b"]
}

variable "private_subnets" {
  description = "Subnets privadas"
  type = list(string)
  default = ["10.0.1.0/24", "10.0.2.0/24"]
}

variable "public_subnets" {
  description = "Subnets públicas"
  type = list(string)
  default = ["10.0.101.0/24", "10.0.102.0/24"]
}
